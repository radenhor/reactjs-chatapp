const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
var pg = require('pg');


const port = 3001;
var app = express();
var server = http.createServer(app);
var io = socketIO(server, {
    serveClient: false,
    pingInterval: 10000,
    pingTimeout: 5000,
    cookie: false
  });
var users = [],isSent = false;
server.listen(port, () => {
    console.log(port, 'is up');
});

var conString = "postgres://rabbit:rabbit+_)@13.229.72.250:5432/chat_db";
var client = new pg.Client(conString);
client.connect();



var sendNotification = function(data) {
    var headers = {
      "Content-Type": "application/json; charset=utf-8"
    };
    
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers
    };
    
    var https = require('https');
    var req = https.request(options, function(res) {  
      res.on('data', function(data) {
        console.log("Response:");
        console.log(JSON.parse(data));
      });
    });
    
    req.on('error', function(e) {
      console.log("ERROR:");
      console.log(e);
    });
    
    req.write(JSON.stringify(data));
    req.end();
  };
  
  
  


async function insertMessage(data){
    await client.query(`insert into chat_message (text,own_id,friend_id) 
    values('${data.text}','${data.user.id}','${data.friendId}')`);
}

findAllUser().then((result)=>{
    for(var i in result.rows){
        let user = {
            socketId : '',
            id : result.rows[i].id,
            name : result.rows[i].name,
            img : result.rows[i].image_url,
            isActive : false
        }
        users.push(user)
    }
})

async function getOnesignalId(id){
    return await client.query(`select onesignal_id from chat_user where id = '${id}'`)
}
async function createNewUser(data){
    await client.query(`insert into chat_user (id,name,image_url) values('${data.id}','${data.name}','${data.img}')`)
}

async function checkExistUser(id){
    return await client.query(`select count(*) from chat_user where id = '${id}'`)
}

async function fetchMessage(ownId,friendId){
    
    return await client.query(`
        select * from chat_message cm inner join chat_user cu on cu.id = cm.own_id where cm.own_id = '${ownId}'  and cm.friend_id = '${friendId}'
        union select * from chat_message cm inner join chat_user cu on cu.id = cm.own_id where cm.own_id = '${friendId}' and cm.friend_id = '${ownId}'
        order by created_date desc
    `)
}

async function findAllUser(){
    return await client.query(`select * from chat_user`);
}

async function addSubscribeUser(data){
    await client.query(`UPDATE chat_user set onesignal_id = '${data.onesignalId}' where id = '${data.userId}'`)
}

function fetchMessageFromServer(ownId,friendId){
    fetchMessage(ownId,friendId).then((result)=>{
        let messages = []
        for(var msg in result.rows){
            let message = {
                id: result.rows[msg].id,
                text: result.rows[msg].text,
                createdAt: result.rows[msg].created_date,
                user: {
                    id: result.rows[msg].own_id,
                    name: result.rows[msg].name,
                    avatar: result.rows[msg].image_url,
                }
            }
            messages.push(message)
        }
        let data = {
            ownId :ownId,
            isSent : isSent,
            friendId : friendId,
            messages : messages
        }
        io.emit('fetchMessage',data)
    })
}

io.on('connection', (socket) => {
    console.log('new user connected');
    socket.on('userJoin',(data)=>{
            for(var i=0;i<users.length;i++){
                if(users[i].id==data.id){
                    users[i].socketId = socket.id
                    users[i].isActive = true
                    io.emit('userJoin',users)
                }
            }
    });
    socket.on('disconnect', () => {
        for(var i=0;i<users.length;i++){
            if(users[i].socketId==socket.id){
                    users[i].isActive = false
                    io.emit('userLeft',users)
                    console.log('User was disconnected')
            }
        }
    });
    socket.on('findAllUser',()=>{
        io.emit('findAllUser',users)
        
    })
    socket.on('onSend', (data) => {
        isSent = true;
        ownId = data[0].user.id
        friendId = data[0].friendId
        console.log(ownId+"/"+friendId)
        insertMessage(data[0]).then(()=>{
            // io.emit('onSend',data[0])
            getOnesignalId(friendId).then((result)=>{
                console.log(result)
                let oneId = result.rows[0].onesignal_id
                var message = { 
                    app_id: "871549ec-8abe-4862-acfb-bc93fc7c0bb1",
                    contents: {"en":  data[0].user.name+" : "+data[0].text},
                    include_player_ids: [""+oneId]
                };
                sendNotification(message);
                fetchMessageFromServer(ownId,friendId)
            })
        })
    });
    socket.on('addSubscribeUser',(data)=>{
        addSubscribeUser(data).then(()=>{
            console.log('updated')
        })
    })
    socket.on('fetchMessage',(data)=>{
        isSent = data.isSent
        fetchMessageFromServer(data.ownId,data.friendId)
    })
    socket.on('createUser',(data)=>{
        checkExistUser(data.id).then((result)=>{
            if(result.rows[0].count=='0'){
                let user = {
                    socketId : socket.id,
                    id : data.id,
                    name : data.name,
                    img : data.picture.data.url,
                    isActive : true,
                }
                createNewUser(user).then(()=>{
                    users.push(user)
                    io.emit('userJoin',users)
                    console.log("created user");
                })
            }
        })
    })
});

