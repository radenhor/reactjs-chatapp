const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
var pg = require('pg');

const port = 3001;
var app = express();
var server = http.createServer(app);
var io = socketIO(server, {
    serveClient: false,
    pingInterval: 10000,
    pingTimeout: 5000,
    cookie: false
  });
var messages = []
var users = []
var roomId = '0',userId = '1';
server.listen(port, () => {
    console.log(port, 'is up');
});

var conString = "postgres://rabbit:rabbit+_)@13.229.72.250:5432/chat_db";
var client = new pg.Client(conString);
client.connect();

async function insertUser(data){
    await client.query(`insert into chat_user (name,image_url,facebook_id) values('${data.name}','${data.img}','${data.id}')`)
}
async function insert(text,id){
    await client.query(`insert into chat_message (text,user_id,room_id) 
    values('${text}',${id},'${roomId}')`);
}
async function insertMessage(text,id){
    checkRoom(roomId).then((result)=>{
        if(result.rowCount==0){
            roomId = generateCode(6);
            insert(text,id);
        }else{
            roomId = result.rows[0].room_id;
            insert(text,id);
        }
    })
    
}
async function checkBeforeFindMessage(){
    return await client.query(`select cu.facebook_id user_id,cu.image_url img,cu.name,cm.id msg_id,cm.text,cm.created_date
                               from chat_message cm inner join
                               chat_user cu on cm.user_id = cu.id where cm.room_id = '${roomId}' or cm.room_id = '${userId}'  
                               order by created_date desc limit 10`)
    
}
async function findAllMessage(roomId){
    checkRoom(roomId).then((result)=>{
        if(result.rowCount==0){
            return null
        }else{
            checkBeforeFindMessage();
        }
    })
}
async function checkRoom(userID){
    console.log("userId",userID)
    checkUser(userID).then((result)=>{
        console.log(result)
        getRoom(result.rows[0].id).then((result)=>{
            roomId = result.rows[0].room_id;
        })
    }) 
}
async function checkUser(id){
    return await client.query(`select * from chat_user where facebook_id = '${id}'`)
}
async function getRoom(id){
    return await client.query(`select cm.room_id from chat_message cm inner join chat_user ch
    on cm.user_id = ch.id where ch.id = ${id} limit 1`)
}
function generateCode(count){
    var chars = 'acdefhiklmnoqrstuvwxyz0123456789'.split('');
    var result = '';
    for(var i=0; i<count; i++){
      var x = Math.floor(Math.random() * chars.length);
      result += chars[x];
    }
    return result;
}
async function checkUserBeforeSend(data){
    checkUser(data.user.id).then((result)=>{
        insertMessage(data.text,result.rows[0].id).then(()=>{
            findAllMessage(result.rows[0].id).then((result)=>{
                if(result!=undefined||result!=null){
                    messages = []
                    console.log(result)
                    for(var i=0;i<result.rows.length;i++){
                        let msg = {
                                id: result.rows[i].msg_id,
                                text: result.rows[i].text,
                                createdAt: result.rows[i].created_date,
                                user: {
                                    id: result.rows[i].user_id,
                                    name: result.rows[i].name,
                                    avatar: result.rows[i].img,
                                }
                        }
                        messages.push(msg)
                    }
                    [""+roomId,""+userId].map((room)=>{
                        io.to(room).emit('onSend',{message:messages})
                    })
                }
            })
        })
    })
}
io.on('connection', (socket) => {
    console.log('new user connected');
    socket.on('join',(data)=>{
        socket.join(data.roomId)
        roomId = data.roomId
        userId = data.userId
        findAllMessage(roomId).then((result)=>{
            console.log(result)
            messages = []
            if(result!=null){
                for(var i=0;i<result.rows.length;i++){
                    let message = {
                            id: result.rows[i].msg_id,
                            text: result.rows[i].text,
                            createdAt: result.rows[i].created_date,
                            user: {
                                id: result.rows[i].user_id,
                                name: result.rows[i].name,
                                avatar: result.rows[i].img,
                            }
                    }
                    messages.push(message)
                }
                [""+roomId,""+userId].map((room)=>{
                    io.to(room).emit('onSend',{message:messages})
                })
            }
        })
    })
    socket.on('userJoin',(data)=>{
        checkUser(data.id).then((result)=>{
            data.socketID = socket.id
            users.push(data)
            
            io.to(roomId).emit('userJoin',users)
            result.rowCount == 0 && insertUser(data).then(()=>{
                console.log("Inserted")
            })
        })        
    });
    socket.on('disconnect', () => {
        var i = users.indexOf(socket.id);
        for(var i=0;i<users.length;i++){
            if(users[i].socketID==socket.id){
                users.splice(i, 1);
                io.to(roomId).emit('userLeft',users)
                console.log('User was disconnected');
            }
        }
    });
    socket.on('onSend', (data) => {
        checkUserBeforeSend(data[0]).then(()=>{
            console.log("Sent")
        })
    });
    socket.on('roomId', (id) => {
        roomId = id
    });
});

