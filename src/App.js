import React from 'react';
import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import ListUser from './ListUser';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import SocketIO from 'socket.io-client';

var remote = require('remote-file-size')

class  App extends React.Component {
  
  state = {
    data : null
  }
  constructor(props){
    super(props);
    const script = document.createElement("script");
    var url = '/Users/raden/Desktop/classroom-cover.jpg'
    remote(url, function(err, o) {
      console.log(o)
      // => 1548
    })
    script.src = "https://cdn.onesignal.com/sdks/OneSignalSDK.js";
    script.type = 'text/javascript'
    script.async = true;
    document.body.appendChild(script);
    this.socket = SocketIO('http://localhost:3001');
    
  }
  responseFacebook = (response) => {
    this.socket.emit('createUser',response)
    this.setState({
      data : response
    })
  }
  render(){
    return (
      this.state.data == null ? 
      <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', height: '100vh'}}>
              <FacebookLogin
              appId="1810051059302448"
              autoLoad
              fields="name,email,picture"
              callback={this.responseFacebook}
              render={renderProps => (
              <a className="fb connect" style={{color:'white'}} onClick={renderProps.onClick}>Login with facebook</a>
              )}
              />
      </div>
      :
      <ListUser data = {this.state.data}></ListUser>
    );
  }
}

export default App;
