import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { ListGroup } from 'react-bootstrap';
import 'react-chat-widget/lib/styles.css';
import { GiftedChat } from 'react-web-gifted-chat';
import SocketIO from 'socket.io-client';

export default class ListUser extends Component {
    
    constructor(props){
       

        var OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
                OneSignal.init({
                appId: "871549ec-8abe-4862-acfb-bc93fc7c0bb1",
                notifyButton: {
                    enable: true,
                },
            });
            
        });
        
        super(props);
        this.state = {
            messages: [],
            users : [],
            isChat : false,
            targetChat : null
        }
        this.ownId = this.props.data.id;
        this.friendId = 0
        this.socket = SocketIO('http://localhost:3001');
        this.socket.on('connect', () => {
            let user = {
                socketId : '',
                id : this.props.data.id,
                name : this.props.data.name,
                img : this.props.data.picture.data.url
              }
            this.socket.emit('userJoin',user)
        });
        this.socket.on('userLeft',(data)=>{
            this.setState({
                users : data
            })
        })
        
        this.socket.on('onSend',(data) => {
            // if(data.friendId==this.ownId){
            //     this.setState(previousState => ({
            //         messages: GiftedChat.append(previousState.messages, data),
            //     }))
            // }
        })
        this.socket.on('userJoin',(data)=>{
            this.setState({ 
                users : data
            })
        })
        this.socket.on('findAllUser',(users)=>{
            this.setState({
                users : users
            })
        })
        this.socket.on('fetchMessage',(data)=>{    
            if((data.friendId==this.ownId&&data.isSent)){
                this.setState({
                    messages: [
                    ],
                });
                this.friendId = data.ownId
                if(this.state.isChat==false){
                    for(var i=0;i<this.state.users.length;i++){
                        if(data.ownId==this.state.users[i].id){
                            this.state.targetChat = this.state.users[i]
                        }
                    }
                }
                this.setState({
                    messages : data.messages
                })
                this.setState({
                    isChat : true
                })
            }else if(data.ownId==this.ownId){
                this.setState({
                    messages: [
                    ],
                });
                this.setState({
                    messages : data.messages
                })
                this.setState({
                    isChat : true
                })
            }
            this.scrollToBottom()
        })
        OneSignal.on('subscriptionChange',  (isSubscribed) => {
            if(isSubscribed){
                OneSignal.getUserId().then((id)=>{
                    let data = {
                        onesignalId : id,
                        userId : this.props.data.id
                    }
                    this.socket.emit('addSubscribeUser',data)
                    console.log(id)
                    console.log('userID',this.props.data.id)
                })
            }
        });
    }
    onSend(messages = []) {
        messages[0].friendId = this.friendId
        messages[0].user.id = this.ownId
        // this.setState(previousState => ({
        //     messages: GiftedChat.append(previousState.messages, messages[0]),
        // }))
        this.socket.emit('onSend',messages)
        this.setState({
            messages: [
            ],
        });
    }
    
    componentDidMount() {
        
       
                       
        // OneSignal.getUserId().then((id)=>console.log(id));
        this.socket.emit('findAllUser')
        this.setState({
            messages: [
            ],
        });
        this.scrollToBottom()
    }
    componentDidUpdate(){
        this.scrollToBottom()
    }
    chatSpecificUser(user){
        this.friendId = user.id;
        let data = {
            ownId : this.ownId,
            friendId : this.friendId,
            isSent : false
        }
        this.socket.emit('fetchMessage',data)
        this.setState({
            isChat : true,
            targetChat : user
        })
    }
    scrollToBottom() {
        var scrollingElement = (document.scrollingElement || document.body); /* you could provide your scrolling element with react ref */
        scrollingElement.scrollTop = scrollingElement.scrollHeight;
    }
    render() {
        return (
            <div className="row">
                <div className="col-md-6">
                    <ListGroup>
                        <ListGroup.Item>
                            <center>
                                <span>List Users</span>
                            </center>
                        </ListGroup.Item>
                        {
                        this.state.users.map((user) => 
                            user.id != this.props.data.id ?
                            (
                            <ListGroup.Item onClick={()=>this.chatSpecificUser(user)}>
                                <span>{user.id}</span>
                                <img style={{borderRadius:25,marginLeft:5,marginRight:5}} src={user.img} width="50" height="50" />
                                <span>{user.name}</span>
                                {user.isActive && <span><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Ski_trail_rating_symbol-green_circle.svg/240px-Ski_trail_rating_symbol-green_circle.svg.png" style={{marginLeft:20}} width="15" height="15"/></span>}
                            </ListGroup.Item>
                            )
                            :
                            ''
                        )
                        }
                    </ListGroup>
                </div>
                <div className="col-md-6">
                    {/* <div className="row"> */}
                        {/* <div className="col-md-12"> */}
                            {
                                this.state.targetChat && 
                                <ListGroup>
                                    <ListGroup.Item>
                                        <center>
                                            <img style={{borderRadius:25,marginLeft:5,marginRight:5}} src={this.state.targetChat.img} width="50" height="50" />
                                            <span>{this.state.targetChat.name}</span>
                                        </center>
                                    </ListGroup.Item>
                                </ListGroup>
                            }
                        {/* </div> */}
                        {/* <div className="col-md-12"> */}
                            {
                                this.state.isChat ?
                                <GiftedChat
                                    messages={this.state.messages}
                                    onSend={(messages) => this.onSend(messages)}
                                    user={{
                                        id: this.props.data.id,
                                        avatar: this.props.data.picture.data.url,
                                        name: this.props.data.name,
                                    }}
                                />
                                : <h1>Welcome to denChat</h1>
                            }
                        {/* </div> */}
                    {/* </div> */}
                </div>
            </div>
        )
    }
}
